module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~cs62160068/learn_bootstrap/'
      : '/'
}
